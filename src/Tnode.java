// http://stackoverflow.com/questions/237159/whats-the-best-way-to-check-to-see-if-a-string-represents-an-integer-in-java
// konsultatsioonid isaga

import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;
   private static Stack<String> strstack; 	
   private static String operaatorid = "/*-+";
   private static String rpn;
   
   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      if (isInteger(name)) {
    	  b.append(name);  
      } else {
    	  b.append(name);
    	  b.append("(");
    	  b.append(firstChild);
    	  if (nextSibling != null) {
    	     b.append(",");
    	     b.append(nextSibling);
    	  }    
    	  b.append(")");
      }
      
      return b.toString();
   }

   
   private static boolean isInteger( String input ) {
	    try {
	        Integer.parseInt( input );
	        return true;
	    }
	    catch( Exception e ) {
	        return false;
	    }
	}
   
   private static void buildNode (Tnode node) {
	   if (strstack.size()==0){
		   throw new RuntimeException("Operaatoril pole piisavalt argumente! Sinu sisestus: " + rpn);
	   }
	   String token = strstack.pop();
	   if (operaatorid.indexOf(token) != -1){
		   node.name=token;
		   node.firstChild = new Tnode();
		   node.nextSibling = new Tnode();
		   buildNode(node.nextSibling);
		   buildNode(node.firstChild); 
	   } else if (isInteger(token)) {
		   node.name=token;  
	   } else {
		   throw new RuntimeException("Avaldises on vigased s�mbolid! Sinu sisestus: " + rpn + " Vigane s�mbol on " + token); 
	   }
   }
   
   public static Tnode buildFromRPN (String pol) {
      Tnode root = null;
      String [] strmas = pol.split(" ");
      strstack = new Stack<String> ();
      for (String s:strmas){
    	  if (s!=" "){
    		  strstack.push(s);
    	  }
      }
      root = new Tnode();
      buildNode(root);
      if (strstack.size()!=0) {
    	  throw new RuntimeException("Liiga palju argumente! Sinu sisestus: " + pol);
     }
      /* if (pol == "	") {
    	  throw new RuntimeException("Tab ei saa p��ratud poola kujul esitleda. Palun sisesta lubatud kujul." + pol);
      } */
      return root;
   } 

   public static void main (String[] param) {
	  rpn = "3 - +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      // TODO!!! Your tests here
   }
}

